package amqp;

import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class QueueUtils {

    private static final Logger logger = LoggerFactory.getLogger(QueueUtils.class);

    public static Connection getConnection() {
        try {

            ConnectionFactory connectionFactory = new ConnectionFactory();
            connectionFactory.setUri("amqp://guest:guest@walletint1.ext.tunubi.com:5672");
            Connection connection = connectionFactory.newConnection();
            logger.info("*** established connection {}", connection);
            return connection;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void send(String queueName, String text, int delayMillis) {
            try {
                logger.info("*** artificial delay {}: {}", queueName, delayMillis);
                Thread.sleep(delayMillis);
                Connection connection = getConnection();
                Channel channel = connection.createChannel();

                channel.basicPublish("", queueName, null, text.getBytes("UTF-8"));
                logger.info("*** sent message {}: {}", queueName, text);

                channel.close();
                connection.close();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

    public static void purgeMessages(String queueName) throws IOException {
        QueueConsumer consumer = new QueueConsumer(queueName);
        consumer.stop();
    }

}
