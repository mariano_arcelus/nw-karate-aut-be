package amqp;

import javax.jms.*;

import com.rabbitmq.client.*;
import com.rabbitmq.client.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Optional;

public class QueueConsumer {

    private static final Logger logger = LoggerFactory.getLogger(QueueConsumer.class);

    private final Connection connection;
    private final String queueName;
    private final Channel channel;

    public QueueConsumer(String queueName) throws IOException {
        this.queueName = queueName;
        this.connection = QueueUtils.getConnection();
        try {
            this.channel = this.connection.createChannel();
        }catch (Exception e) {
            throw e;
        }
    }

    public void listen() {

        try {
            boolean autoAck = true;
            channel.basicConsume(queueName, autoAck, "KarateConsumer",
                    new DefaultConsumer(channel) {
                        @Override
                        public void handleDelivery(String consumerTag,
                                                   Envelope envelope,
                                                   AMQP.BasicProperties properties,
                                                   byte[] body)
                                throws IOException
                        {
                            String routingKey = envelope.getRoutingKey();
                            logger.info("Routing key: {} ", routingKey);
                            String contentType = properties.getContentType();
                            logger.info("Content type: {} ", contentType);
                            long deliveryTag = envelope.getDeliveryTag();
                            // (process the message components here ...)
                            channel.basicAck(deliveryTag, false);
                        }
                    });
            connection.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void stop() {
        try {
            connection.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
