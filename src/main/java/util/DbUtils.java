package util;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;
import org.json.JSONArray;
import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DbUtils {
    private static final Logger logger = LoggerFactory.getLogger(DbUtils.class);
    private Connection pgConnection;
    private Properties props;

    public DbUtils(Map<String, Object> config) throws SQLException {
        String url = (String) config.get("url");
        Properties props = new Properties();
        props.setProperty("user", (String) config.get("username"));
        props.setProperty("password", (String) config.get("password"));
        props.setProperty("ssl","false");
        try {
            this.pgConnection = DriverManager.getConnection(url, props);
        }catch (java.sql.SQLException e) {
            throw e;
        }
    }

    public String readValue(String query) throws Exception {
        Statement st = pgConnection.createStatement();
        ResultSet rs = st.executeQuery(query);
        String result = convertToJSON(rs).toString();
        pgConnection.close();
        return result;
    }

    public static JSONArray convertToJSON(ResultSet resultSet) throws Exception {
        JSONArray jsonArray = new JSONArray();
        while (resultSet.next()) {
            int total_rows = resultSet.getMetaData().getColumnCount();
            JSONObject obj = new JSONObject();
            for (int i = 0; i < total_rows; i++) {
                if ( resultSet.getObject(i + 1) != null ) {
                    Object value = resultSet.getObject(i + 1);
                    obj.put(resultSet.getMetaData().getColumnLabel(i + 1)
                            .toLowerCase(), value);
                }
            }
            jsonArray.put(obj);
        }
        return jsonArray;
    }
}
