package util;

import com.mongodb.MongoCredential;
import com.mongodb.client.*;
import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.Map;

public class MongoUtils {

    public static MongoDatabase database;

    public MongoUtils( Map<String, Object> config ) {

        // Creating a Mongo client
        MongoClient mongoClient = MongoClients.create( "mongodb://" + config.get("url") + ":" + config.get("port") );

        // Creating Credentials
        MongoCredential credential;
        credential = MongoCredential.createCredential((String) config.get("username"), (String) config.get("db"),
                ((String) config.get("password")).toCharArray());
        System.out.println("Connected to the database successfully");

        // Accessing the database
        MongoDatabase database = mongoClient.getDatabase((String) config.get("db"));
        System.out.println("Credentials ::"+ credential);
    }

    public static MongoCollection<Document> readCollection(String collectionName) {
        // Retieving a collection
        MongoCollection<Document> collection = database.getCollection(collectionName);
        System.out.println("Collection myCollection selected successfully");
        return collection;
    }

    public JSONArray readAllDocs(String collectionName) {
        // Retrieving a collection
        MongoCollection<Document> collection = readCollection(collectionName);
        System.out.println("Collection sampleCollection selected successfully");

        // Getting the iterable object
        FindIterable<Document> iterDoc = collection.find();
        int i = 1;

        // Getting the iterator
        Iterator it = iterDoc.iterator();

        JSONArray jsonArray = new JSONArray();
        while (it.hasNext()) {
            JSONObject obj = new JSONObject();
            obj.put(it.next().toString(),true);
            System.out.println(it.next());
            i++;
            jsonArray.put(obj);
        }
        return jsonArray;
    }
}
