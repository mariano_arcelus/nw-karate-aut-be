Feature: Test rabbitMQ

  Background:
    * def QueueUtils = Java.type('amqp.QueueUtils')
    * def QueueConsumer = Java.type('amqp.QueueConsumer')
    * def queueName = QUEUE_NAME
    * def queue = new QueueConsumer(queueName)
#    * def handler = function(msg){ karate.signal(msg) }


  Scenario: Send Message to RabbitMQ
    * string json  = { transactionId: '136ace31-304ba5-8d8e6d67a2c7b2223', userFrom: '136ace31-3029-4ba5-8d82-e6d67a2c7b90', methodFrom: '20123123233', methodTo: '136ace31-3029-4ba5-8d82-e6d67a2c7b91', methodToType: '90123123123', amount: 1799, currencyCode: 'ARS', status: 'OK', methodFromType: 'P2P_SENDING' }
    * eval QueueUtils.send(queueName, json, 25)
    * eval queue.listen()
