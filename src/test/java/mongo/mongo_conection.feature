Feature: Connect to Mongo DB

  Background:
    * url PROTOCOL + baseUrl + ':' + REG_PORT

  Scenario: Connect and serach on Mongo DB
    # use jdbc to get the jwt confirmation token
    * def config = { username: '#(MONGO_DB_USER)', password: '#(MONGO_DB_PASS)', url: '#(MONGO_DB_URI)', port: '#(MONGO_DB_PORT)', db: '#(MONGO_DB)' }
    * def getMongoDocs =
"""
function(config) {
  var MongoUtils = Java.type('util.MongoUtils');
  var db = new MongoUtils(config);
  return JSON.parse(db.readAllDocs('mongodb'));
}"""
    * def mongoDocs = call getMongoDocs config
#    * print jwt_token[0].email_confirmation_jwt
    Given path REG_CONFIRM + '/' + jwt_token[0].email_confirmation_jwt
    And header Authorization = 'Bearer ' + AUTH_TOKEN
    And header Content-type = 'application/json'
    When method get
    Then status 200
