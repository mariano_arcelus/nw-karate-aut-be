var config = { // base config JSON
    appId: 'ID',
    appSecret: 'SECRET',
    random_id: random_digits(8),

    MONGO_DB_USER: "user",
    MONGO_DB_PASS: "pass",
    MONGO_DB_URI: "walletint1.ext.tunubi.com",
    MONGO_DB_PORT: "27017",
    MONGO_DB: "mongoDB",

    AUTH_TOKEN: '',
    PROTOCOL: 'http://',
    baseUrl: qa_url,
    DB_PORT: qa_DB_PORT,
    REG_PORT: 7002,
    USR_PORT: 7003,
    AUT_PORT: 7005,
    TRA_PORT: 7006,
    REL_PORT: 7007,

    REG_DB_URI: 'jdbc:postgresql://' + qa_url + ':' + qa_DB_PORT + '/registration?tcpKeepAlive=true',
    REG_DB_USER: 'postgres',
    REG_DB_PASS: '',
    REG_PATH: '/registration/email',
    REG_CONFIRMED: '/registration/confirmed',
    REG_CONFIRM: '/registration/confirm',
    REG_CUIL: '/registration/cuil',
    REG_STATUS: '/registration/status',

    USER_DB_URI: 'jdbc:postgresql://' + qa_url + ':' + qa_DB_PORT + '/users?tcpKeepAlive=true',
    USER_DB_USER: 'postgres',
    USER_DB_PASS: '',
    USER_USERNAME_RESERVATION: '/registration/username_reservations',
    USER_PHONE_REGISTRATION: '/registration/phone_numbers',
    USER_PHONE_CONFIRMATION: '/registration/phone_numbers/confirm',
    USER_PIN_GENERATION: '/registration/pin/validate',
    USER_REG_COMPLETE: '/registration/complete',
    USERS: '/users',
    USER_STATUS: '/users/status',
    USER_REGISTERED_PASSWORD: 'Test-0000',


    AUTH_DB_URI: 'jdbc:postgresql://' + qa_url + ':' + qa_DB_PORT + '/auth?tcpKeepAlive=true',
    AUTH_DB_USER: 'postgres',
    AUTH_DB_PASS: '',
    AUTH_CREDENTIALS: '/auth/credentials',
    AUTH_STATUS: '/auth/status',
    AUTH_LOGIN: '/auth/login',
    AUTH_PIN_VALIDATE: '/auth/credentials/pin/validate',

    TRANSACTIONS: '/transactions',
    TRANSACTION_STATUS: '/transactions/status',

    RELATIONS_STATUS: '/relations/status'
  };