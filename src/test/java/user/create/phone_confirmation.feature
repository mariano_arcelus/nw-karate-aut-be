@UserCreation
Feature: Phone confirmation

  Background:
    * url PROTOCOL + baseUrl + ':' + REG_PORT

  Scenario: Phone confirmation
    * def config = { username: '#(REG_DB_USER)', password: '#(REG_DB_PASS)', url: '#(REG_DB_URI)', driverClassName: 'org.postgresql.Driver' }
    * def getPhoneConfirmCode =
"""
function(config) {
  var DbUtils = Java.type('util.DbUtils');
  var db = new DbUtils(config);
  return JSON.parse(db.readValue('select sms_confirmation_code from phone_data where phone_number = \'+54911999' + RANDOM_PHONE + '\''));
}
"""
    * def phone_config_code = call getPhoneConfirmCode config
    * print phone_config_code
    * def confCode = phone_config_code[0].sms_confirmation_code
    * def randomPhoneNumber = '+54911999' + RANDOM_PHONE
    Given path USER_PHONE_CONFIRMATION
    And header Authorization = 'Bearer ' + AUTH_TOKEN
    And request { code: #(confCode), phone_number: #(randomPhoneNumber) }
    When method post
    Then status 200