@UserCreation
Feature: Cuil reservation

  Background:
    * url PROTOCOL + baseUrl + ':' + REG_PORT
    * def CUIL = cuil
    * print random_id

  Scenario: Cuil reservation
    Given path REG_CUIL
    And header Authorization = 'Bearer ' + AUTH_TOKEN
    And header Content-type = 'application/json'
    And request { cuil: '#(CUIL)' }
    When method post
    Then status 201
