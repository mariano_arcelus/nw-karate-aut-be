@UserCreation
Feature: Username reservation

  Background:
    * url PROTOCOL + baseUrl + ':' + REG_PORT

  Scenario: Username reservation
    Given path USER_USERNAME_RESERVATION
    And header Authorization = 'Bearer ' + AUTH_TOKEN
    * def randomUser = 'Karate' + RANDOM_USER
    * def randomPass = USER_REGISTERED_PASSWORD
    And request { username:'#(randomUser)', password:'#(randomPass)' }
    When method post
    Then status 201
