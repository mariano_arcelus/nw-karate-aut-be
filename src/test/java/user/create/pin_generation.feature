@UserCreation
Feature: Pin generation

  Background:
    * url PROTOCOL + baseUrl + ':' + REG_PORT

  Scenario: Pin generation
    Given path USER_PIN_GENERATION
    And header Authorization = 'Bearer ' + AUTH_TOKEN
    And request { pin: '#(RANDOM_PIN)' }
    When method post
    Then status 200
