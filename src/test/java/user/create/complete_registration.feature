@UserCreation
Feature: Complete registration

  Background:
    * url PROTOCOL + baseUrl + ':' + REG_PORT

  Scenario: Complete registration
    Given path USER_REG_COMPLETE
    And header Authorization = 'Bearer ' + AUTH_TOKEN
    And request { name: 'KARATE', last_name: 'TEST', sex_type: 'M', dni: '#(DNI)', ppe: false, terms: true }
    When method post
    Then status 201