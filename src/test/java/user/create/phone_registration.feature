@UserCreation
Feature: Phone registration

  Background:
    * url PROTOCOL + baseUrl + ':' + REG_PORT

  Scenario: Phone registration
    * def randomPhoneNumber = '+54911999' + RANDOM_PHONE
    Given path USER_PHONE_REGISTRATION
    And header Authorization = 'Bearer ' + AUTH_TOKEN
    And request { phone_number: '#(randomPhoneNumber)' }
    When method post
    Then status 201
