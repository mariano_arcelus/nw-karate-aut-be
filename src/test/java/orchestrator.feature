Feature: Test orchestration feature

  Scenario: Run all of the tests in sequence

#    * call read('create_user.feature')
#    * call read('confirm_email.feature')
    * call read('user/create/cuil_reservation.feature')
    * call read('user/create/username_reservation.feature')
    * call read('user/create/phone_registration.feature')
    * call read('user/create/phone_confirmation.feature')
    * call read('user/create/pin_generation.feature')
    * call read('user/create/complete_registration.feature')