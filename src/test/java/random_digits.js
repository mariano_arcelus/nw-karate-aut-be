function random_digits(s) {
      var text = "";
      var possible = "123456789";
      for (var i = 0; i < s; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      return text;
  }