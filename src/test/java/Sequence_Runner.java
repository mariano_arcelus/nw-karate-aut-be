import com.intuit.karate.junit4.Karate;
import cucumber.api.CucumberOptions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

@RunWith(Karate.class)
@CucumberOptions(features = "classpath:orchestrator.feature")
public class Sequence_Runner {

        @BeforeClass
        public static void beforeClass() {
            System.out.println("BEFORE");
        }

        @AfterClass
        public static void afterClass() {
            System.out.println("AFTER");
        }
}
