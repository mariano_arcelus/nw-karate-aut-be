Feature: Create user in Users

  Background:
    * url PROTOCOL + baseUrl + ':' + REG_PORT
    * def mail = 'karate-' + random_id + '@yopmail.com'

  Scenario: Auth token
    Given path REG_PATH
    And header Content-type = 'application/json'
    And request { email: '#(mail)', device_id: '1a0093f4-b4b6-4b88-9024-KARATE-TESTS', platform: 'ANDROID' }
    When method post
    Then status 201
    And match response == { id_token: '#regex ^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$' }

    * def token = $.id_token
