Feature: Confirm email

  Background:
    * url PROTOCOL + baseUrl + ':' + REG_PORT

  Scenario: Confirmed email false
    Given path REG_CONFIRMED
    And header Authorization = 'Bearer ' + AUTH_TOKEN
    And header Content-type = 'application/json'
    And request { id_token: '#(AUTH_TOKEN)' }
    When method get
    Then status 200
    And match response.email_confirmed == false

  Scenario: Confirm email
    # use jdbc to get the jwt confirmation token
    * def config = { username: '#(REG_DB_USER)', password: '#(REG_DB_PASS)', url: '#(REG_DB_URI)', driverClassName: 'org.postgresql.Driver' }
    * def getJwtToken =
"""
function(config) {
  var DbUtils = Java.type('util.DbUtils');
  var db = new DbUtils(config);
  return JSON.parse(db.readValue('select * from email_registration where email = \'' + EMAIL + '\''));
}"""
    * def jwt_token = call getJwtToken config
#    * print jwt_token[0].email_confirmation_jwt
    Given path REG_CONFIRM + '/' + jwt_token[0].email_confirmation_jwt
    And header Authorization = 'Bearer ' + AUTH_TOKEN
    And header Content-type = 'application/json'
    When method get
    Then status 200

  Scenario: Confirmed email true
    Given path REG_CONFIRMED
    And header Authorization = 'Bearer ' + AUTH_TOKEN
    And header Content-type = 'application/json'
    And request { id_token: '#(AUTH_TOKEN)' }
    When method get
    Then status 200
    And match response == { email_confirmed: true, id_token: '#regex ^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$' }

    * def token = $.id_token
