function fn() {

  // Path for running common feature scripts from any Runner
  var testsPath = '\\build\\resources\\test\\';
  var currentPath = new java.io.File("").getAbsolutePath();
  var fullPath = currentPath + testsPath;

  var env = karate.env; // get java system property 'karate.env'

  var random_digits = read(fullPath + 'random_digits.js');
  var getCuil = read(fullPath + 'get_cuil.js');

  karate.log('karate.env system property was:', env);

  if (!env) {
    env = 'qa'; // a custom 'intelligent' default
  }

  var qa_url = 'walletint1.ext.tunubi.com'
  var qa_DB_PORT = '5434'

  var config = { // base config JSON
    appId: 'ID',
    appSecret: 'SECRET',
    random_id: random_digits(8),

    MONGO_DB_USER: "user",
    MONGO_DB_PASS: "pass",
    MONGO_DB_URI: "walletint1.ext.tunubi.com",
    MONGO_DB_PORT: "27017",
    MONGO_DB: "mongoDB",

    AUTH_TOKEN: '',
    PROTOCOL: 'http://',
    baseUrl: qa_url,
    DB_PORT: qa_DB_PORT,
    REG_PORT: 7002,
    USR_PORT: 7003,
    AUT_PORT: 7005,
    TRA_PORT: 7006,
    REL_PORT: 7007,

    REG_DB_URI: 'jdbc:postgresql://' + qa_url + ':' + qa_DB_PORT + '/registration?tcpKeepAlive=true',
    REG_DB_USER: 'postgres',
    REG_DB_PASS: '',
    REG_PATH: '/registration/email',
    REG_CONFIRMED: '/registration/confirmed',
    REG_CONFIRM: '/registration/confirm',
    REG_CUIL: '/registration/cuil',
    REG_STATUS: '/registration/status',

    USER_DB_URI: 'jdbc:postgresql://' + qa_url + ':' + qa_DB_PORT + '/users?tcpKeepAlive=true',
    USER_DB_USER: 'postgres',
    USER_DB_PASS: '',
    USER_USERNAME_RESERVATION: '/registration/username_reservations',
    USER_PHONE_REGISTRATION: '/registration/phone_numbers',
    USER_PHONE_CONFIRMATION: '/registration/phone_numbers/confirm',
    USER_PIN_GENERATION: '/registration/pin/validate',
    USER_REG_COMPLETE: '/registration/complete',
    USERS: '/users',
    USER_STATUS: '/users/status',
    USER_REGISTERED_PASSWORD: 'Test-0000',


    AUTH_DB_URI: 'jdbc:postgresql://' + qa_url + ':' + qa_DB_PORT + '/auth?tcpKeepAlive=true',
    AUTH_DB_USER: 'postgres',
    AUTH_DB_PASS: '',
    AUTH_CREDENTIALS: '/auth/credentials',
    AUTH_STATUS: '/auth/status',
    AUTH_LOGIN: '/auth/login',
    AUTH_PIN_VALIDATE: '/auth/credentials/pin/validate',

    TRANSACTIONS: '/transactions',
    TRANSACTION_STATUS: '/transactions/status',

    RELATIONS_STATUS: '/relations/status'
  };

  if (env == 'dev') {
    // over-ride only those that need to be
    config.baseUrl = 'walletint1.ext.tunubi.com';
    config.REG_DB_URI = 'jdbc:postgresql://' + config.baseUrl + ':' + config.DB_PORT + '/registration?tcpKeepAlive=true';
    config.USER_DB_URI = 'jdbc:postgresql://' + config.baseUrl + ':' + config.DB_PORT + '/users?tcpKeepAlive=true';
    config.AUTH_DB_URI = 'jdbc:postgresql://' + config.baseUrl + ':' + config.DB_PORT + '/auth?tcpKeepAlive=true';
  }

  config.QUEUE_NAME = 'p2p_request_queue';

  function generateUUID() { // Public Domain/MIT
    var d = new Date().getTime();
    if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
        d += performance.now(); //use high-precision timer if available
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
  }

  config.UUID = generateUUID();
  config.RANDOM_PHONE = random_digits(5);
  config.RANDOM_USER = random_digits(5);
  config.RANDOM_PIN = random_digits(4);
  config.DNI = '254' + config.RANDOM_USER;

  karate.log('---------------- DNI : ', config.DNI);

  config.cuil = getCuil(config.random_id);

  karate.log('---------------- FULL PATH : ', fullPath);

  karate.log();

  // DET for token
  var result = karate.callSingle(fullPath + 'common/create_user.feature', config);
  config.AUTH_TOKEN = result.token;
  config.EMAIL = result.mail;

  // don't waste time waiting for a connection or if servers don't respond within 5 seconds
  karate.configure('connectTimeout', 5000);
  karate.configure('readTimeout', 5000);

  karate.log('random_id:', config.random_id);
  karate.log('AUTH_TOKEN:', config.AUTH_TOKEN);

  var result_confirm = karate.callSingle(fullPath + 'common/confirm_email.feature', config);
  config.AUTH_TOKEN = result_confirm.token;


  return config;
}