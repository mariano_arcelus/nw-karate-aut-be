function getCuil(arg) {
    var cuil = arg;
    var xy = '20';
    var vector = [3,2,7,6,5,4,3,2];
    var sumatoria = 10;
    for (var i = 0; i < 8; i++) {
        sumatoria += arg[i] * vector[i];
    }
    var digito_verificador = 11 - ( sumatoria % 11 );
    if (digito_verificador == 11) {
        digito_verificador = 0;
    }
    else if (digito_verificador == 10) {
        digito_verificador = 9;
        xy = '23';
    }
    cuil = xy + cuil + digito_verificador;
    return cuil;
}